﻿using NUnit.Framework;
using OpenQA.Selenium;
using SeleniumAutomation.utils;
using SeleniumAutomation.drivers;

namespace SeleniumAutomation.tests
{
    public class BaseTest
    {
    protected IWebDriver? Driver;
    protected dynamic? Config;

    [OneTimeSetUp]
    public void GlobalSetup()
    {
        Config = ConfigurationManager.LoadConfig("qa");
        Driver = WebDriverManager.GetDriver("edge");
    }

    [OneTimeTearDown]
    public void GlobalTeardown()
    {
        WebDriverManager.QuitDriver();
    }

    [TearDown]
    public void TakeScreenshotOnFailure()
    {
        if (TestContext.CurrentContext.Result.Outcome.Status == NUnit.Framework.Interfaces.TestStatus.Failed)
        {
            var screenshot = ((ITakesScreenshot)Driver).GetScreenshot();
            var fileName = $"{TestContext.CurrentContext.Test.Name}_{DateTime.Now:yyyyMMdd_HHmmss}.png";
            var filePath = Path.Combine("C:\\Users\\flori\\source\\repos\\CreateNewP\\create-new-paste\\work\\Selenium1\\Screenshots\\", fileName);
            screenshot.SaveAsFile(filePath);
        }
    }
}
}
