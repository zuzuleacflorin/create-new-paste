using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Support.UI;
using SeleniumAutomation.pages;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SeleniumAutomation.tests
{
    public class CreateNewPaste1
    {
        private IWebDriver driver;

        [SetUp]
        public void InitScript()
        {

            driver = new ChromeDriver();
        }

        [Test]
        public void Task1()
        {
            PasteBinMainPage hp = new(driver);

            driver.Navigate().GoToUrl("https://pastebin.com/");
            driver.Manage().Window.Maximize();
            hp.AgreeCookies();
            hp.PasteCode.SendKeys("Hello from WebDriver");
            hp.PasteExpirationDropDown.Click();
            hp.PasteExpiration10Min.Click();
            hp.PasteName.SendKeys("helloweb");
            hp.CreateNewPaste.Click();
        }

        [TearDown]
        public void Cleanup()
        {
            driver.Quit();
        }

    }
}