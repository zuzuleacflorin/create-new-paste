﻿using OpenQA.Selenium.Chrome;
using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OpenQA.Selenium.Support.UI;
using SeleniumAutomation.pages;

namespace SeleniumAutomation.tests
{
    [TestFixture]
    public class GoogleCalculatorTest : BaseTest
    {
        private GoogleCloudPage? googleCloud;

        [SetUp]
        public void SetUp()
        {
            googleCloud = new GoogleCloudPage(Driver);
            Driver.Navigate().GoToUrl(Config.url.ToString());
            Driver.Manage().Window.Maximize();
        }
        [Test]
        public async Task Task3Async()
        {
            googleCloud.LandingInput.Click();
            googleCloud.LandingInput.SendKeys("Google Cloud Platform Pricing Calculator");
            googleCloud.LandingInput.SendKeys(Keys.Enter);
            googleCloud.ClickFirstResult();
            googleCloud.AddToEstimate.Click();
            googleCloud.ClickComputeEngine();
            googleCloud.SetNumberOfInstances("4");
            googleCloud.CloseCookie();
            googleCloud.ScrollToElement(googleCloud.MachineType);
            googleCloud.MachineType.Click();
            googleCloud.N1standard8.Click();
            googleCloud.AddGPUs.Click();
            googleCloud.GPUModelExpandDropDown();
            googleCloud.NVIDIATeslaV100.Click();
            googleCloud.LocalSSD.Click();
            googleCloud.LocalSSD2x375();
            googleCloud.RegionDropDown.Click();
            googleCloud.RegionEuWest4.Click();
            googleCloud.CommittedUsageOptions1Year.Click();
            await Task.Delay(1000);
            Assert.That(string.IsNullOrEmpty(googleCloud.EstiamteCostValue.Text), Is.False, "The element text should not be null or empty.");
            googleCloud.ShareButton.Click();
            googleCloud.ClickOpenEstimateSummary();
            googleCloud.SwitchToTheLastTab();
            googleCloud.CheckSummary();
        }
    }
}
