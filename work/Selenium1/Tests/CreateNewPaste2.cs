﻿using OpenQA.Selenium.Chrome;
using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OpenQA.Selenium.Support.UI;
using SeleniumExtras.WaitHelpers;
using SeleniumAutomation.pages;

namespace SeleniumAutomation.tests
{
    internal class CreateNewPaste2
    {
        private IWebDriver _driver;

        [SetUp]
        public void InitScript()
        {

            _driver = new ChromeDriver();
        }

        [Test]
        public async Task Task2Async()
        {
            string url = "https://pastebin.com/";
            string pasteName = "how to gain dominance among developers";
            string firstLine = "git config --global user.name \"New Sheriff in Town\"";
            string secondsLine = """git reset $(git commit-tree HEAD^{tree} -m "Legacy code")""";
            string thirdLine = "git push origin master --force";


            PasteBinMainPage hp = new(_driver);

            _driver.Navigate().GoToUrl(url);
            _driver.Manage().Window.Maximize();
            await Task.Delay(500);
            hp.AgreeCookies();
            hp.ScrollToElement(hp.PasteCode);
            hp.PasteCode.SendKeys(firstLine + "\n" + secondsLine + "\n" + thirdLine);
            await Task.Delay(500);
            hp.CloseAdd();
            hp.SyntaxHighlightingDropDown.Click();
            hp.SyntaxHighlightingBash.Click();
            hp.PasteExpirationDropDown.Click();
            hp.PasteExpiration10Min.Click();
            hp.ScrollToElement(hp.CreateNewPaste);
            hp.PasteName.SendKeys(pasteName);
            hp.CreateNewPaste.Click();
            _driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(10);
            await Task.Delay(1000);
            string actualTitle = _driver.Title;
            Assert.That(actualTitle, Does.Contain(pasteName), "Page title does not match the expected title");
            //Syntax is suspended for bash - Need more clarification regarding this requirement
            hp.CheckCodeText(hp.FirstCodeLine, firstLine);
            hp.CheckCodeText(hp.SecondCodeLine, secondsLine);
            hp.CheckCodeText(hp.ThirdCodeLine, thirdLine);

        }
        [TearDown]
        public void Cleanup()
        {
            _driver.Quit();
        }
    }
}