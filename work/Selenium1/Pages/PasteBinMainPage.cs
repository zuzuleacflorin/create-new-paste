﻿using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Interactions;
using OpenQA.Selenium.Support.UI;
using SeleniumExtras.PageObjects;
using SeleniumExtras.WaitHelpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SeleniumAutomation.pages
{
    public class PasteBinMainPage
    {
        private readonly IWebDriver _driver;

        public PasteBinMainPage(IWebDriver driver)
        {
            _driver = driver;
            PageFactory.InitElements(driver, this);
        }

        public void AgreeCookies()
        {
            WebDriverWait wait = new(_driver, TimeSpan.FromSeconds(10));
            IWebElement element = wait.Until(ExpectedConditions.ElementExists(By.XPath("//div[@class='qc-cmp2-summary-buttons']//button[@mode='primary']")));
            element.Click();
        }

        public void CloseAdd()
        {
            IWebElement popUp = _driver.FindElement(By.Id("hideSlideBanner"));
            if (popUp.Displayed)
            {
                popUp.Click();
            }
        }
        [FindsBy(How = How.Id, Using = "postform-text")]
        public IWebElement PasteCode { get; set; }

        [FindsBy(How = How.XPath, Using = "//span[@id='select2-postform-expiration-container']")]
        public IWebElement PasteExpirationDropDown { get; set; }

        [FindsBy(How = How.XPath, Using = "//li[text()='10 Minutes']")]
        public IWebElement PasteExpiration10Min { get; set; }

        [FindsBy(How = How.Id, Using = "select2-postform-format-container")]
        public IWebElement SyntaxHighlightingDropDown { get; set; }

        [FindsBy(How = How.XPath, Using = "(//li[text()='Bash'])[1]")]
        public IWebElement SyntaxHighlightingBash { get; set; }

        [FindsBy(How = How.XPath, Using = "//input[@id='postform-name']")]
        public IWebElement PasteName { get; set; }

        [FindsBy(How = How.XPath, Using = "//button[text()='Create New Paste']")]
        public IWebElement CreateNewPaste { get; set; }

        [FindsBy(How = How.XPath, Using = "//div[@class='notice -success -post-view']")]
        public IWebElement NoteS { get; set; }

        public void ValidatePaste(string expctedTest)
        {
            _driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(1);
            Assert.That(NoteS.Text, Does.Contain(expctedTest));
        }

        public void ScrollToElement(IWebElement element)
        {
            Actions actions = new(_driver);
            actions.MoveToElement(element).Perform();
        }

        public void WaitForElementPresent(By locator)
        {
            WebDriverWait wait = new WebDriverWait(_driver, TimeSpan.FromSeconds(10));
            wait.Until(ExpectedConditions.ElementToBeClickable(locator));
        }

        [FindsBy(How = How.XPath, Using = "//ol[@class='bash']/li[1]/div")]
        public IWebElement FirstCodeLine { get; set; }

        [FindsBy(How = How.XPath, Using = "//ol[@class='bash']/li[2]/div")]
        public IWebElement SecondCodeLine { get; set; }

        [FindsBy(How = How.XPath, Using = "//ol[@class='bash']/li[3]/div")]
        public IWebElement ThirdCodeLine { get; set; }


        public void CheckCodeText(IWebElement element, string expectedText)
        {
            var jsExecutor = (IJavaScriptExecutor)_driver;
            string script = @"
                var parent = arguments[0];
                var text = '';
                var childNodes = parent.childNodes;
                for (var i = 0; i < childNodes.length; i++) {
                    if (childNodes[i].nodeType === Node.TEXT_NODE) {
                        text += childNodes[i].textContent;
                    } else if (childNodes[i].nodeType === Node.ELEMENT_NODE) {
                        text += childNodes[i].innerText;
                    }
                }
                return text;";
            string combinedText = (string)jsExecutor.ExecuteScript(script, element);
            Assert.That(combinedText.Trim, Is.EqualTo(expectedText), "The concatenated text does not match the expected text.");


        }
    }
}
