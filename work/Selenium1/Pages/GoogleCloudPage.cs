﻿using OpenQA.Selenium.Chrome;
using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SeleniumExtras.PageObjects;
using OpenQA.Selenium.Support.UI;
using SeleniumExtras.WaitHelpers;
using OpenQA.Selenium.Interactions;

namespace SeleniumAutomation.pages
{
    internal class GoogleCloudPage
    {
        private readonly IWebDriver driver;

        public GoogleCloudPage(IWebDriver driver)
        {
            this.driver = driver;
            PageFactory.InitElements(driver, this);
        }

        [FindsBy(How = How.XPath, Using = "//input[@class='mb2a7b']")]
        public IWebElement LandingInput { get; set; }

        public void ClickFirstResult()
        {
            WebDriverWait wait = new(driver, TimeSpan.FromSeconds(10));
            IWebElement element = wait.Until(ExpectedConditions.ElementExists(By.XPath("(//div[@class='gsc-webResult gsc-result']//a)[1]")));
            element.Click();
        }

        [FindsBy(How = How.XPath, Using = "(//span[text()='Add to estimate'])[1]")]
        public IWebElement AddToEstimate { get; set; }

        public void ClickComputeEngine()
        {
            WebDriverWait wait = new(driver, TimeSpan.FromSeconds(10));
            IWebElement element = wait.Until(ExpectedConditions.ElementExists(By.XPath("//h2[text()='Compute Engine']")));
            element.Click();
        }

        public void SetNumberOfInstances(string value)
        {
            WebDriverWait wait = new(driver, TimeSpan.FromSeconds(10));
            IWebElement element = wait.Until(ExpectedConditions.ElementExists(By.XPath("//div[@class='QiFlid']//input")));
            element.Clear();
            element.SendKeys(value);
        }

        [FindsBy(How = How.XPath, Using = "(//span[text()='Machine type']//ancestor::span)[1]//parent::div")]
        public IWebElement MachineType { get; set; }

        public void ScrollToElement(IWebElement element)
        {
            Actions actions = new(driver);
            actions.MoveToElement(element).Perform();
        }

        public void CloseCookie()
        {
            IWebElement popUp = driver.FindElement(By.XPath("//button[@class='glue-cookie-notification-bar__accept']"));
            if (popUp.Displayed)
            {
                popUp.Click();
            }
        }

        [FindsBy(How = How.XPath, Using = "//li[@data-value='n1-standard-8']")]
        public IWebElement N1standard8 { get; set; }

        [FindsBy(How = How.XPath, Using = "//button[@aria-label='Add GPUs']")]
        public IWebElement AddGPUs { get; set; }

        [FindsBy(How = How.XPath, Using = "//span[text()='GPU Model']//ancestor::span//parent::div")]
        public IWebElement GPUModel { get; set; }

        public void GPUModelExpandDropDown()
        {
            WebDriverWait wait = new(driver, TimeSpan.FromSeconds(10));
            IWebElement element = wait.Until(ExpectedConditions.ElementExists(By.XPath("//span[text()='GPU Model']//ancestor::span//parent::div")));
            element.Click();
        }

        [FindsBy(How = How.XPath, Using = "//li[@data-value='nvidia-tesla-v100']")]
        public IWebElement NVIDIATeslaV100 { get; set; }

        [FindsBy(How = How.XPath, Using = "//span[text()='Local SSD']//ancestor::span//parent::div")]
        public IWebElement LocalSSD { get; set; }

        public void LocalSSD2x375()
        {
            WebDriverWait wait = new(driver, TimeSpan.FromSeconds(10));
            IWebElement element = wait.Until(ExpectedConditions.ElementExists(By.XPath("//span[text()='2x375 GB']//ancestor::li")));
            element.Click();
        }

        [FindsBy(How = How.XPath, Using = "//span[text()='Region']//ancestor::span//parent::div")]
        public IWebElement RegionDropDown { get; set; }

        [FindsBy(How = How.XPath, Using = "//li[@data-value='europe-west4']")]
        public IWebElement RegionEuWest4 { get; set; }

        [FindsBy(How = How.XPath, Using = "//input[@id='1-year']/parent::div")]
        public IWebElement CommittedUsageOptions1Year { get; set; }

        [FindsBy(How = How.XPath, Using = "//div[text()='Estimated cost']/parent::div/child::div/label")]
        public IWebElement EstiamteCostValue { get; set; }

        [FindsBy(How = How.XPath, Using = "//button[@aria-label='Open Share Estimate dialog']")]
        public IWebElement ShareButton { get; set; }

        public void ClickOpenEstimateSummary()
        {
            WebDriverWait wait = new(driver, TimeSpan.FromSeconds(10));
            IWebElement element = wait.Until(ExpectedConditions.ElementExists(By.XPath("//a[text()='Open estimate summary']")));
            element.Click();
        }

        [FindsBy(How = How.XPath, Using = "//button[@aria-label='Open Share Estimate dialog']")]
        public IWebElement CommittedUsage1Year { get; set; }

        public void SwitchToTheLastTab()
        {
            IReadOnlyList<string> windowHandles = driver.WindowHandles;
            driver.SwitchTo().Window(windowHandles[windowHandles.Count - 1]);

        }


        [FindsBy(How = How.XPath, Using = "//span[text()='Number of Instances']/following-sibling::span")]
        public IWebElement NumberOfInstancesSUmmary { get; set; }

        [FindsBy(How = How.XPath, Using = "//span[text()='Operating System / Software']/following-sibling::span")]
        public IWebElement OperatingSystemSummary { get; set; }

        [FindsBy(How = How.XPath, Using = "//span[text()='Provisioning Model']/following-sibling::span")]
        public IWebElement ProvisioningModelSummary { get; set; }

        [FindsBy(How = How.XPath, Using = "//span[text()='Machine type']/following-sibling::span")]
        public IWebElement MachimeTypeSUmmary { get; set; }

        [FindsBy(How = How.XPath, Using = "//span[text()='GPU Model']/following-sibling::span")]
        public IWebElement GPUTypeSummary { get; set; }

        [FindsBy(How = How.XPath, Using = "//span[text()='Number of GPUs']/following-sibling::span")]
        public IWebElement NumberofGPUSummary { get; set; }

        [FindsBy(How = How.XPath, Using = "//span[text()='Local SSD']/following-sibling::span")]
        public IWebElement LocalSSDSUmmary { get; set; }

        [FindsBy(How = How.XPath, Using = "//span[text()='Region']/following-sibling::span")]
        public IWebElement DatacenterLocationSummary { get; set; }

        [FindsBy(How = How.XPath, Using = "//span[text()='Committed use discount options']/following-sibling::span")]
        public IWebElement CommittedUsage { get; set; }

        public void CheckSummary()
        {
            WebDriverWait wait = new(driver, TimeSpan.FromSeconds(10));
            wait.Until(_driver =>
                {
                    return ((IJavaScriptExecutor)driver).ExecuteScript("return document.readyState").ToString().Equals("complete");
                });

            var fieldsToCheck = new Dictionary<IWebElement, string>
    {
        { NumberOfInstancesSUmmary, "4" },
        { OperatingSystemSummary, "Free: Debian, CentOS, CoreOS, Ubuntu or BYOL (Bring Your Own License)" },
        { ProvisioningModelSummary, "Regular" },
        { MachimeTypeSUmmary, "n1-standard-8, vCPUs: 8, RAM: 30 GB" },
        { GPUTypeSummary, "NVIDIA V100" },
        { NumberofGPUSummary, "1" },
        { LocalSSDSUmmary, "2x375 GB" },
        { DatacenterLocationSummary, "Netherlands (europe-west4)" },
        { CommittedUsage, "1 year" }
    };

            foreach (var field in fieldsToCheck)
            {
                IWebElement fieldId = field.Key;
                string expectedText = field.Value;

                Assert.That(fieldId.Text.Trim, Is.EqualTo(expectedText), $"The text of element with ID {fieldId} does not match the expected text.");
            }

        }
    }


}
