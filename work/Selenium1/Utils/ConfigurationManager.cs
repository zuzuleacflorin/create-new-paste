﻿using Microsoft.Extensions.Configuration;
using System.IO;
using System;
using Newtonsoft.Json;

namespace SeleniumAutomation.utils
{
    public  static class ConfigurationManager
    {
        public static dynamic LoadConfig(string environment)
        {
            var configJson = File.ReadAllText("C:\\Users\\flori\\source\\repos\\CreateNewP\\create-new-paste\\work\\Selenium1\\config.json");
            dynamic config = JsonConvert.DeserializeObject(configJson);
            return config[environment];
        }
    }
}
