﻿using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Edge;

namespace SeleniumAutomation.drivers
{
    public static class WebDriverManager
    {
        private static IWebDriver? driver;

        public static IWebDriver GetDriver(string browser)
        {
            driver ??= browser.ToLower() switch
                {
                    "chrome" => new ChromeDriver(),
                    "edge" => new EdgeDriver(),
                    _ => throw new ArgumentException("Unsupported browser: " + browser),
                };
            return driver;
        }

        public static void QuitDriver()
        {
            driver?.Quit();
        }
    }
}
